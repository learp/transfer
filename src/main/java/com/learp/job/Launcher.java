package com.learp.job;

import com.learp.job.http.server.JettyServer;
import com.learp.job.http.server.Server;
import com.learp.job.http.server.config.CommonServerConfiguration;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Launcher {
    private static final String APP_NAME = "Tranferer";
    private static final int DEFAULT_JETTY_PORT = 8080;

    public static void main(String[] args) {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> log.info("{} stopped", APP_NAME)));

        CommonServerConfiguration config = CommonServerConfiguration.custom().port(getPort()).build();
        Server server = new JettyServer(config);

        try {
            log.info("{} starting...", APP_NAME);
            server.start();
            log.info("{} started [port = {}]", APP_NAME, config.getPort());
        }
        catch (Exception exception) {
            log.error("Error occurred during launch: ", exception);
            handleLaunch(server);
        }
        finally {
            server.join();
        }
    }

    private static void handleLaunch(Server server) {
        try {
            server.stop();
        } catch (Exception exception) {
            log.error("Error occurred during stop: ", exception);
        }
    }

    private static int getPort() {
        return Integer.getInteger("transferer.port", DEFAULT_JETTY_PORT);
    }
}
