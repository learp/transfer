package com.learp.job.http.annotations;

import org.eclipse.jetty.http.HttpMethod;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Mapping {
    HttpMethod method() default HttpMethod.GET;
    String url() default "";
}
