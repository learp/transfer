package com.learp.job.http.api;

import com.learp.job.http.annotations.Controller;
import com.learp.job.http.annotations.Mapping;
import org.eclipse.jetty.http.HttpMethod;

@Controller(baseUrl = "transfer")
public class TransferController {
    @Mapping(method = HttpMethod.GET)
    public void transferw(TransferRequest request) {
        System.out.println("!wow! " + request.a);
    }

    @Mapping(method = HttpMethod.POST)
    public void transfer(TransferRequest request) {
        System.out.println("!wow! " + request.a);
    }
}
