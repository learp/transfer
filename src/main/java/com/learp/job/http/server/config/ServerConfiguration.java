package com.learp.job.http.server.config;

/**
 * @author Golovanov Egor (Egor.Golovanov@billing.ru)
 */
public interface ServerConfiguration {
    int getPort();
}
