package com.learp.job.http.server;

/**
 * @author Golovanov Egor (Egor.Golovanov@billing.ru)
 */
public interface Server {
    void start() throws Exception;
    void join();
    void stop() throws Exception;
}
