package com.learp.job.http.server;

import com.learp.job.http.annotations.Controller;
import com.learp.job.http.annotations.Mapping;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import org.eclipse.jetty.http.HttpMethod;
import org.reflections.Reflections;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class DispatcherServlet extends HttpServlet {

    Map<String, Object> context = new HashMap<>();
    Map<MappingKey, Handler> mapping = new HashMap<>();

    @Override
    public void init() throws ServletException {
        Reflections reflections = new Reflections("com.learp.job.http.api");
        Set<Class<?>> controllers = reflections.getTypesAnnotatedWith(Controller.class);

        for (Class<?> clazz : controllers) {
            Controller controllerAnnotation = clazz.getAnnotation(Controller.class);
            String baseUrl = controllerAnnotation.baseUrl();

            try {
                Object controller = clazz.getDeclaredConstructor().newInstance();

                Method[] declaredMethods = clazz.getMethods();
                for (Method declaredMethod : declaredMethods) {
                    Mapping mappingAnnotation = declaredMethod.getAnnotation(Mapping.class);
                    if (mappingAnnotation != null) {
                        mapping.put(
                                new MappingKey(normalizePath(baseUrl + "/" + mappingAnnotation.url()), mappingAnnotation.method()),
                                new Handler(declaredMethod, controller));
                    }
                }
            } catch (IllegalAccessException | NoSuchMethodException e) {
                throw new ServletException("Controller " + clazz.getCanonicalName() + " must have no args public constructor");
            } catch (InstantiationException e) {
                throw new ServletException("Can't create controller " + clazz.getCanonicalName() + ": " + e.getMessage());
            } catch (InvocationTargetException e) {
                throw new ServletException(e);
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest req, HttpServletResponse resp) {
        MappingKey mappingKey = new MappingKey(normalizePath(req.getPathInfo()), HttpMethod.fromString(req.getMethod()));
        Handler handler = mapping.get(mappingKey);

        if (handler == null) {
            try {
                resp.sendError(404, "Not found");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private String normalizePath(String path) {
        StringBuilder result = new StringBuilder(path.length());

        for (int i = 0; i < path.length(); i++) {
            char c = path.charAt(i);
            result.append(c);

            if (c != '/') {

            }

            while (c == '/' && i < path.length()) {
                c = path.charAt(i);
                i++;
            }
        }

        if (path.length() > 1 && path.endsWith("/")) {
            result.deleteCharAt(result.length() - 1);
        }

        return result.toString();
    }

    @AllArgsConstructor
    @EqualsAndHashCode
    private class MappingKey {
        String url;
        HttpMethod method;
    }

    @AllArgsConstructor
    private class Handler {
        Method method;
        Object controller;

        public Object invoke() {
            return null;
        }
    }
}
