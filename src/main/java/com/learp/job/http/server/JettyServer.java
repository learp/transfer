package com.learp.job.http.server;

import com.learp.job.http.server.config.ServerConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import java.io.IOException;

import static org.eclipse.jetty.util.resource.Resource.newClassPathResource;

/**
 * @author Golovanov Egor (Egor.Golovanov@billing.ru)
 */
@Slf4j
public class JettyServer implements Server {

    private final ServerConfiguration config;
    private org.eclipse.jetty.server.Server server;

    public JettyServer(ServerConfiguration config) {
        this.config = config;
    }

    @Override
    public void start() throws Exception {
        server = new org.eclipse.jetty.server.Server(config.getPort());
        setupConnector();

        server.setHandler(buildServletContextHandler());
        server.start();
    }

    @Override
    public void join() {
        try {
            server.join();
        } catch (InterruptedException ignore) {}
    }

    @Override
    public void stop() throws Exception {
        server.stop();
    }

    // ****************************** //

    private void setupConnector() {
        ServerConnector connector = new ServerConnector(server);

        connector.setPort(config.getPort());
        connector.setAcceptQueueSize(400);

        server.setConnectors(new Connector[]{connector});
    }

    ContextHandler buildServletContextHandler() throws IOException {
        ServletContextHandler contextHandler = new ServletContextHandler();

        contextHandler.addServlet(new ServletHolder(new DispatcherServlet()), "/*");
        return contextHandler;
    }

}
