package com.learp.job.http.server.config;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Golovanov Egor (Egor.Golovanov@billing.ru)
 */
@Builder(builderMethodName = "custom")
@Getter
public class CommonServerConfiguration implements ServerConfiguration {
    public static final CommonServerConfiguration DEFAULT_CONFIG = new CommonServerConfiguration(8080);
    private int port;
}
